// JavaScript Document

var timeout         = 100;
var closetimer		= 0;
var ddmenuitem      = 0;

function jsddm_open(e)
{
	$(".elem a").removeClass("curr");
	$(this.firstChild).addClass("curr");
	jsddm_canceltimer();
	jsddm_close();
	ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');}

function jsddm_close()
{	
	if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');
	
}

function jsddm_timer()
{	closetimer = window.setTimeout(jsddm_close, timeout);
	var cls = $(this.firstChild).attr("class");
	if(cls.indexOf("joinUs") ==-1){
		$(".elem a").removeClass("curr");
		$(".now").addClass("curr");
	}
	//$(".joinUs").addClass("curr");
}

function jsddm_canceltimer()
{	if(closetimer)
	{	window.clearTimeout(closetimer);
		closetimer = null;}}

$(document).ready(function()
{	$('#jsddm > li').bind('mouseover', jsddm_open);
	$('#jsddm > li').bind('mouseout',  jsddm_timer);});

document.onclick = jsddm_close;
