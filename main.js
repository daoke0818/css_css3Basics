/**
 * Created by Administrator on 2017/2/27.
 */
$(function () {
    var menuHtml = "\
        <div id='menu'>\
        <h1><small>导 航</small></h1>\
        <h4><a class='toHome' href='../index.html'> 回首页</a></h4>\
    <section >\
    <h2>选择器</h2>\
    <div class='subMenu'>\
        <a href='../01selector/01attrSelector.html'>属性选择器</a>\
        <a href='../01selector/02StructuralPseudoClassSelector1.html'>结构性伪类选择器1</a>\
        <a href='../01selector/03StructuralPseudoClassSelector2.html'>结构性伪类选择器2</a>\
        <a href='../01selector/04UIelementStatusPseudoClassSelector.html'>UI元素状态伪类选择器</a>\
        <a href='../01selector/05brotherElementSelector.html'>兄弟选择器</a>\
        </div>\
        <h2>使用选择器在页面中插入内容</h2>\
        <div class='subMenu'>\
        <a href='../02useSelector/01insertTextAndImg.html'>插入文本和图片</a>\
        <a href='../02useSelector/02insertSerialNumber.html'>插入编号</a>\
        </div>\
        <h2>文字与字体相关样式</h2>\
        <div class='subMenu'>\
        <a href='../03font/01textShadow.html'>文字阴影——text-shadow</a>\
    <a href='../03font/02wordWrap.html'>文本换行——word-break</a>\
    <a href='../03font/03serverFonts.html'>使用服务端字体——Web Font</a>\
    <a href='../03font/04clientFonts.html'>显示客户端字体</a>\
        <a href='../03font/05useRem.html'>使用rem单位</a>\
        </div>\
        <h2>盒相关样式</h2>\
        <div class='subMenu'>\
        <a href='../04boxStyle/01basicBoxTypes.html'>盒的基本类型</a>\
        <a href='../04boxStyle/02overflow.html'>溢出控制</a>\
        <a href='../04boxStyle/03boxShadow.html'>盒阴影</a>\
        <a href='../04boxStyle/04calculateWidthAndHeight.html'>改变元素的宽高计算方法</a>\
        </div>\
        <h2>背景和边框相关样式</h2>\
        <div class='subMenu'>\
        <a href='../../05backgroundAndBorder/01backgroundAttr.html'>背景的新增属性</a>\
        <a href='../05backgroundAndBorder/02gradientBackground.html'>渐变背景</a>\
        <a href='../05backgroundAndBorder/03borderRadius.html'>圆角边框的绘制</a>\
        <a href='../05backgroundAndBorder/04borderImg.html'>使用边框图像</a>\
        </div>\
        <h2>CSS3中的变形处理</h2>\
        <div class='subMenu'>\
        <a href='../06_2dAnd3d/01_2d.html'>2D变形</a>\
    <a href='../06_2dAnd3d/02_3d.html'>3D变形</a>\
    <a href='../06_2dAnd3d/03transformatioMatrix.html'>变形矩阵</a>\
        </div>\
        <h2>CSS3中的动画功能</h2>\
        <div class='subMenu'>\
        <a href='../07animation/01Transitions.html'>Transitions动画</a>\
        <a href='../07animation/02animations.html'>animations动画</a>\
        </div>\
        <h2>布局相关样式</h2>\
        <div class='subMenu'>\
        <a href='../08layout/01multicolumnLayout.html'>多列布局</a>\
        <a href='../08layout/02boxLayout.html'>盒布局</a>\
        <a href='../08layout/03flexBox.html'>弹性盒布局1</a>\
        <a href='../08layout/04flexBox2.html'>弹性盒布局2</a>\
        <a href='../08layout/05flexBox3.html'>弹性盒布局3</a>\
        <a href='../08layout/06flexBox4(lineAlign).html'>弹性盒布局4</a>\
        <a href='../08layout/07calc.html'>calc计算方法</a>\
        </div>\
        <h2>媒体查询相关样式</h2>\
        <div class='subMenu'>\
        <a href='../09mediaQueries/01chooseDifferentStyleAccordingToWindowWidth.html'>根据不同浏览器宽度选择样式</a>\
        </div>\
        <h2>CSS3其他重要样式和属性</h2>\
        <div class='subMenu'>\
        <a href='../10others/01color.html'>颜色相关样式</a>\
        <a href='../10others/02UserInterfaceStyleAndinitial.html'>用户界面相关样式和initial</a>\
        <a href='../10others/03FilterEffects.html'>图像滤镜效果</a>\
        </div>\
        <h2>综合练习</h2>\
        <div class='subMenu'>\
        <a href='../11syntheticInstance/01aHomePageLayout/index.html'>某网站首页</a>\
        <a href='../11syntheticInstance/02TheOrderLayout/index.html'>某订单界面</a>\
        </div>\
        </section>\
        </div>\
        ";
    $("body").prepend(menuHtml);
    var $menu = $("#menu");
    $menu.find("a:not(.toHome)").hide();

    if (isMobile.any()){//移动设备
        $menu.find("h2").click(function () {
            $(this).next().find("a").toggle();
        })
    }else{//电脑屏幕
        $menu.find("h2").click(function () {
            $(this).next().find("a").hide(300)
        }).hover(function () {
            $(this).next().find("a").delay(300).show(200)
                .parent().siblings("div").find("a").stop(true,true).hide(300);
        });
    }

    var $style = $("head>style");
    var styleHtmlDiv = $style.html().replace("  div{","  div:not(#menu):not(.subMenu){ ");
    $style.html(styleHtmlDiv);



});
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i) ? true: false;
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i) ? true: false;
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true: false;
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) ? true: false;
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
    }
};
